import logo from './logo.svg';
import './App.css';
import LayoutComponent from './BaitapLayoutComponent/LayoutComponent/LayoutComponents';

function App() {
  return (
    <LayoutComponent></LayoutComponent>
  );
}

export default App;
