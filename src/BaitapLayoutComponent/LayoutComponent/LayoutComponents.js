import React from "react";
import Header from "../Header/Header";
import Body from "../Body/Body";
import Footer from "../Footer/Footer";

export default function LayoutComponent() {
    return (
        <section>
            {/* Header */}
            <Header> </Header>
            {/* Body */}
            <Body></Body>
            {/* Footer */}
            < Footer ></Footer >
        </section>
    )
}
