import React, { Component } from "react";

export default class Item extends Component {

    ListItem = () => {
        return [1, 2, 3, 4, 5].map(() => {
            return (
                <div className="col mb-5" >
                    <div className="card w-100" style={{ width: "15rem", }}>
                        <img src="./logo512.png" className="card-img-top d-block w-100" alt="..." />
                        <div class="card-body text-center">
                            <h5 className="card-title">Card title</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" className="btn btn-primary">Find Out More</a>
                        </div>
                    </div>
                </div>
            )
        })
    }


    render() {
        return (
            <section className="pt-4">
                <div className="container px-lg-5">
                    <div className="row  row-cols-sm-1 row-cols-md-2 row-cols-lg-4">
                        {/* <div class="col mb-5">
                            <div class="card w-100" style={{ width: "15rem", }}>
                                <img src="..." class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div> */}
                        {this.ListItem()}

                    </div>
                </div>
            </section>
        );
    }
}